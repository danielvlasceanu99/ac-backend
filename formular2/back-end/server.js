const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament2"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50) UNIQUE KEY,facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20), sex VARCHAR(10))";
    connection.query(sql, function (err, result) {
        if (err) {throw err};
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let sex = req.body.sex;
    let rows = req.body.rows;
    let error = [];
    
  //Validare nume.
  if (!nume){
    console.log("Nu ati introdus numele.");
    error.push("Nu ati introdus numele.");
} else if (nume.length < 3 || nume.length > 30) {
    console.log("Numele este invalid!");
    error.push("Numele este invalid");
   } else if (!nume.match("^[A-Za-z]+$")) {
     console.log("Numele trebuie sa contina doar litere!");
     error.push("Numele trebuie sa contina doar litere!");
   }
  //Validare prenume.
   if(!prenume){
     console.log("Nu ati introdus prenumele.");
     error.push("Nu ati introdus prenumele.");
   } else if (prenume.length < 3 || prenume.length > 30) {
    console.log("Prenumele este invalid!");
    error.push("Prenumele este invalid!");
  } else if (!prenume.match("^[A-Za-z]+$")) {
    console.log("Prenumele trebuie sa contina doar litere!");
    error.push("Prenumele trebuie sa contina doar litere!");
  }
    
  //Validare telefon.
  if(!telefon){
    console.log("Nu ati introdus numarul de telefon.");
    error.push("Nu ati introdus numarul de telefon.");
  } else if (telefon.length != 10) {
    console.log("Numarul de telefon trebuie sa contina 10 cifre!");
    error.push("Numarul de telefon trebuie sa contina 10 cifre!");
  } else if (!telefon.match("^[0-9]+$")) {
    console.log("Numarul de telefon trebuie sa contina doar cifre!");
    error.push("Numarul de telefon trebuie sa contina doar cifre!");
  }
  
  //Validare email.
  if(!email){
    console.log("Nu ati introdus adresa de email.");
    error.push("Nu ati introdus adresa de email.");
  } else  if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
    console.log("Adresa de email este invalida!");
    error.push("Adresa de email este invalida!");
  }

  //Validare facebook.
  if(!facebook){
    console.log("Nu ati introdus contul de facebook.");
    error.push("Nu ati introdus contul de facebook.");
  } else if (!facebook.includes("web.facebook.com/")){
   console.log("Contul de facebook nu este valid.");
   error.push("Contul de facebook nu este valida.");
  }

  //validare abonament.
  if(!tipAbonament) {
    console.log("Nu ati Introdus tipul abonamentului.");
    error.push("Nu ati introdus tipul abonamentului.");
  }
  
  //Validare numar card.
  if(!nrCard){
    console.log("Nu ati introdus numarul cardului.");
    error.push("Nu ati introdus numarul cardului.");
  } else if (nrCard.length != 16) {
   console.log("Numarul cardului nu este valid.");
   error.push("Numarul cardului nu este valid.");
  } else if(!nrCard.match("^[0-9]+$")) {
    console.log("Numarul cardului trebuie sa contina doar cifre.");
    error.push("Numarul cardului trebuie sa contina doar cifre.");
  }
  
  //Validare cvv.
  if(!cvv){
    console.log("Nu ati introdus cvv.");
    error.push("Nu ati introdus cvv.");
  } else if (cvv.length!=3) {
   console.log("Cvv nu este valid.");
   error.push("Cvv nu este valid.");
  } else if(!cvv.match("^[0-9]+$")) {
    console.log("Cvv trebuie sa contina doar cifre.");
    error.push("Cvv trebuie sa contina doar cifre.");
  }
  
  //validare varsta.
  if(!varsta){
    console.log("Nu ati introdus varsta.");
    error.push("Nu ati introdus varsta.");
  } else if(varsta.length < 1 || varsta.lenght > 3){
    console.log("Varsta nu este valida.");
    error.push("varsta nu este valida.");
  } else if(!varsta.match("^[0-9]+$")) {
    console.log("Varsta trebue sa contina doar cifre.");
    error.push("Varsta trebuie sa contina doar cifre.");
  }
  
  //validare CNP.
  if(!cnp){
    console.log("Nu ati introdus CNP-ul.");
    error.push("Nua ti introdus CNP-ul.");
  } else if(!cnp.match("^[0-9]+$")){
    console.log("CNP-ul trebuie sa contina doar cifre.");
    error.push("CNP-ul trebuie samcontina doar cifre.");
  } else if(cnp.length != 13){
    console.log("CNP invalid.");
    error.push("CNP invalid.");
  }

  //Adaugare sex.
  if(cnp.substring(0, 1) == 1 || cnp.substring(0, 1) == 3 || cnp.substring(0, 1) == 5) { sex = "Masculin"; }
  if(cnp.substring(0, 1) == 2 || cnp.substring(0, 1) == 4 || cnp.substring(0, 1) == 6) { sex = "Feminin"; }
        
  //Comparare CNP si varsta.
  if(cnp.substring(0,1) == 1 || cnp.substring(0,1) == 2 )
    if(119 - cnp.substring(1, 3) != varsta && 119 - cnp.substring(1, 3) != varsta - 1){
      console.log("Varsta si CNP-ul nu se potrivesc.");
      error.push("Varsta si CNP-ul nu se potrivesc.");
  }
  if(cnp.substring(0,1) == 3 || cnp.substring(0,1) == 4)
    if(219 - cnp.substring(1, 3) == varsta || 219 - cnp.substring(1, 3) == varsta - 1){
      console.log("Varsta si CNP-ul nu se potrivesc.");
      error.push("Varsta si CNP-ul nu se potrivesc.");
  } 
  if(cnp.substring(0,1) == 6 || cnp.substring(0,1) == 6)
    if(19 - cnp.substring(1, 3) == varsta || 19 - cnp.substring(1, 3) == varsta - 1){
       console.log("Varsta si CNP-ul nu se potrivesc.");
       error.push("Varsta si CNP-ul nu se potrivesc.");
  }
  if(error.lenght === 0){
  const Sql = 'SELECT COUNT(*) AS count FROM abonament WHERE email = ?'
    connection.query(Sql, [email],  function(err,rows) {
       if (err) throw err; 
       if(rows != 0) {
         console.log("Emailul este deja folosit.");
         res.status(400).send({
          message: "Adresa de email exista deja !"
      });
       }
      });
    }
    if(error.length === 0) {
    const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv +"','" +varsta +"','" +cnp +"','"+sex +"')";
    connection.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Abonament achizitionat!");
  
         
            res.status(200).send({
              message: "Abonament achizitionat!"
          });
          console.log(sql);
          
        });
  } else {
    res.status(500).send(error)
    console.log("Eroare la inserarea in baza de date!");
  }
});